import os
import logging

from discord import Intents
from discord import Status
from discord import Game
from discord.ext import commands
from date_time_helper import DateTimeHelper


class Client(commands.Bot):
  """Custom díscord bot class"""
  def __init__(self, command_prefix, *, intents: Intents, log:logging, bot_info:dict):
    super().__init__(command_prefix, intents=intents)
    self.log:logging = log
    self.start_time:float = DateTimeHelper.get_unix_time()
    self.version:str = bot_info['version']
    self.date:str = bot_info['date']

  async def startup(self):
    await self.wait_until_ready()

  async def count_guilds(self):
    """Count the number of guilds, where the bot is joined"""
    return len(self.guilds)

  async def update_presence(self):
    """Update the presence"""
    await self.change_presence(activity=Game(name=f"v{self.version} | {await self.count_guilds()}"), status=Status.online)
    self.log.info("Updated presence")

  async def get_uptime(self):
    """Returns the uptime in seconds"""
    time_now = DateTimeHelper.get_unix_time()
    return int(round(time_now - self.start_time, 0))

  async def load_cogs(self):
    """Load all cog commands"""
    for filename in os.listdir("./cogs"):
      if filename.endswith("py") and filename != "__init__.py":
        await self.load_extension(f"cogs.{filename[:-3]}")
