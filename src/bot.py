import logging
import asyncio
import os

import discord
from discord import Guild
from discord import app_commands
from discord import Interaction

from client import Client
import config_handler

bot_info = {'version': '0.0.1', 'date': '04.04.2024'}

logger = logging.getLogger('discord')
logger.setLevel(logging.DEBUG)

logging.getLogger('discord.http').setLevel(logging.INFO)

handler = logging.FileHandler(filename='data/log/discord.log', encoding='utf-8', mode='a')
formatter = logging.Formatter('%(asctime)s|%(levelname)s|%(name)s|:%(message)s')

handler.setFormatter(formatter)
logger.addHandler(handler)

bot = Client(intents=discord.Intents.all(), command_prefix="!", log=logger, bot_info=bot_info)


@bot.event
async def on_ready():
  logger.info(f"Logged in as: {bot.user.name} with ID {bot.user.id}")
  await bot.load_cogs()
  await asyncio.sleep(1)
  synced = await bot.tree.sync()
  logger.info(f"Synced {len(synced)} command(s)")
  await bot.update_presence()


@bot.event
async def on_guild_join(guild: Guild):
  logger.info("Joined guild")
  logger.info(f"Guilds: {len(bot.guilds)}")
  await bot.update_presence()


@bot.event
async def on_guild_remove(guild: Guild):
  logger.info("Left guild")
  logger.info(f"Guilds: {len(bot.guilds)}")
  await bot.update_presence()


@bot.event
async def on_app_command_error(interaction: Interaction, error):
  if isinstance(error, app_commands.MissingPermissions):
    await interaction.response.send_message(content="You need the rights, to use this command", ephemeral=True)
  else:
    raise error


bot.run(config_handler.get_bot_token())
