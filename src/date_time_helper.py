import datetime
import time


class DateTimeHelper:
  @staticmethod
  def get_time_now():
    date_now = datetime.datetime.now()
    return str(date_now).split(" ")[0]

  @staticmethod
  def get_unix_time():
    return time.time()

  @staticmethod
  def get_date():
    date_now = datetime.datetime.now()
    return str(date_now).split(" ")

