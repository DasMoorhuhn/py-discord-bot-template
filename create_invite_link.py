import sys

permissions = "274877966400"
scope = "bot+applications.commands"

try: client_id = sys.argv[1]
except:
  print("Put the client id from the discord bot behind this command")
  exit(1)

url = f"https://discord.com/api/oauth2/authorize?client_id={client_id}&permissions={permissions}&scope={scope}"

print(url)
