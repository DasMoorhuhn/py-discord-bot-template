FROM python:3.12-alpine

WORKDIR /src

COPY requirements.txt requirements.txt
RUN /usr/local/bin/python -m pip install --upgrade pip
RUN pip3.12 install -r requirements.txt
RUN rm requirements.txt

COPY ./src/ .

RUN mkdir -p ./data/log
RUN mkdir -p ./data/config

CMD [ "python3.12", "bot.py" ]
